package net.wg.gui.lobby.store.data
{
    import net.wg.data.VO.ButtonPropertiesVO;

    public class UpgradeModuleVO extends ButtonPropertiesVO
    {

        public var moduleId:int = -1;

        public function UpgradeModuleVO(param1:Object)
        {
            super(param1);
        }
    }
}
